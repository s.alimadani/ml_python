# Practical machine learning in Python #
This is the repository for the scripts used for the course Practical Machine Learning in Python.

There are two directories, one for the scripts used within each session and the other as example solutions to the homework assignments.
